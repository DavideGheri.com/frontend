import Api from '../Api';
import { Dispatch } from 'redux';
import * as types from '../actionTypes';
import { HomepageModel } from '../models/homepage';

const homepageContentFetched = (homepage: HomepageModel) => ({
  type: types.HOMEPAGE_CONTENT_FETCHED,
  payload: homepage
});

export const getHomepageContent = () => (dispatch: Dispatch) => {
  Api.getAll('homepage')
    .then((res: any) => dispatch(homepageContentFetched(res)));
};
