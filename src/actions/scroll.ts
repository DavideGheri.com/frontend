import * as types from '../actionTypes';
import { Action } from '../models';

const SCROLL_OFFSET = 300;
const SCROLL_OFFSET_MOBILE = 10;

const MOBILE_BREAKPOINT = 768;

const hasScrolled = (offset: number | string) => {
  const width = window.innerWidth
    || document.documentElement.clientWidth
    || document.body.clientWidth;
  if (width <= MOBILE_BREAKPOINT) {
    return offset >= SCROLL_OFFSET_MOBILE;
  }
  return offset >= SCROLL_OFFSET
};

export const scrollUp = (offset: number | string): Action => ({
  type: types.SCROLL_UP,
  payload: {offset, scrolled: hasScrolled(offset)}
});

export const scrollDown = (offset: number | string): Action => ({
  type: types.SCROLL_DOWN,
  payload: {offset, scrolled: hasScrolled(offset)}
});