import Api from '../Api';
import { Dispatch } from 'redux';
import * as types from '../actionTypes';
import { Skills } from '../models/skill';

const languageFetched = (languages: Skills) => ({
  type: types.LANGUAGE_FETCHED,
  payload: languages
});

export const getLanguages = () => (dispatch: Dispatch) => {
  Api.getAll('language')
    .then((res: any) => dispatch(languageFetched(res)));
};
