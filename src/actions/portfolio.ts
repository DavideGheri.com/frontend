import Api from '../Api';
import { Dispatch } from 'redux';
import { PortfolioModel, Portfolios } from '../models/portfolio';
import * as types from '../actionTypes';

const portfolioFetched = (portfolios: Portfolios) => ({
  type: types.PORTFOLIO_FETCHED,
  payload: portfolios
});

const portfolioFound = (portfolio: PortfolioModel) => ({
  type: types.PORTFOLIO_FOUND,
  payload: portfolio
});

const nextPrevFound = (res: {next: PortfolioModel, prev: PortfolioModel}) => ({
  type: types.NEXT_PREV_FOUND,
  payload: res
});

export const getPortfolios = () => (dispatch: Dispatch) => {
  Api.getAll('portfolio')
    .then((res: Portfolios) => dispatch(portfolioFetched(res)));
};

export const getPortfolio = (id: number) => (dispatch: Dispatch) => {
  Api.get('portfolio', id)
    .then((res: PortfolioModel) => dispatch(portfolioFound(res)));
};

export const getNextPrevPortfolio = (id: number) => (dispatch: Dispatch) => {
  Api.getNextPrev('portfolio', id)
    .then((res: {next: PortfolioModel, prev: PortfolioModel}) => dispatch(nextPrevFound(res)))
};
