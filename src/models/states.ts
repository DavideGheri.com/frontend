import { PortfolioModel, Portfolios } from './portfolio';
import { HomepageModel } from './homepage';
import { History, Location } from 'history';
import { Skills } from './skill';

export interface GlobalProps {
  getLanguages: () => void;
  getNextPrevPortfolio: (id: string | number) => void;
  getPortfolio: (id: string | number) => void;
  getPortfolios: () => void;
  push: (path: string) => any;
  scrollDown: () => void;
  scrollUp: () => void;
}

export interface GlobalState {
  scroll: Scroll;
  portfolio: Portfolio;
  language: Language;
  homepage: Homepage;
  router: any;
}

export interface PageProps extends GlobalProps {
  history: History;
  location: Location;
  match: any;
  staticContext: any;
}

export interface Scroll {
  scrolled: boolean;
  offset: number;
}

export interface Portfolio {
  portfolios: Portfolios;
  active?: PortfolioModel | null;
  next?: PortfolioModel | null;
  prev?: PortfolioModel | null;
}

export interface Language {
  languages: {
    client?: Skills,
    server?: Skills,
  }
}

export interface Homepage {
  content: HomepageModel | null
}