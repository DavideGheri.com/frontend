import { Taxonomy } from './taxonomy';

export interface Term {
  id: number;
  title: string;
  content_count: number;
  description: string;
  taxonomy: Taxonomy;
}
