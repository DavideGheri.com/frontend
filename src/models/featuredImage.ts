
export interface FeaturedImage {
  filename: string;
  collection_name: string;
  id: number;
  model: any;
  name: string;
  filesize: number;
  full_url: string;
  url: string;
  path: string;
  sizes?: string;
  srcSet?: string;
  width?: string;
}