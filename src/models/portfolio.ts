import { Tag } from './tag';
import { FeaturedImage } from './featuredImage';

export interface PortfolioModel {
  id: number;
  title: string;
  content?: string;
  featured_image?: FeaturedImage | null;
  published_at: string;
  excerpt?: string;
  json_content?: string;
  url?: string;
  tags?: Tag[];

  fields: {
    content: string;
    featured_image: FeaturedImage;
    url: string;
    excerpt?: string;
  }
}

export type Portfolios = PortfolioModel[];