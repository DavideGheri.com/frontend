import { Term } from './term';

export enum SkillType {
  client = 'client',
  server = 'server',
}

export interface SkillModel {
  id: number;
  title: string;
  fields: {
    image: {
      filename: string;
      collection_name: string;
      id: number;
      model: any;
      name: string;
      filesize: number;
      full_url: string;
      url: string;
      path: string;
    };
    link: string;
    type: SkillType
  },
  terms: {
    [key: string]: Term[]
  }
}

export type Skills = SkillModel[];