
export interface LanguageModel {
  id: number;
  name: string;
  logo: string;
  description: string;
  link: string;
}

export type Languages = LanguageModel[];