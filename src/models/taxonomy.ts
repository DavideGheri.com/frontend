
export interface Taxonomy {
  id: number;
  name: string;
  slug: string;
  term_count: number;
  description: string;
}