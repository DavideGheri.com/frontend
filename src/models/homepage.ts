
export interface HomepageModel {
  id: number;
  title: string;
  fields: {
    'who-am-i': string;
    'contacts': string;
    'contacts-image': string;
  }
}