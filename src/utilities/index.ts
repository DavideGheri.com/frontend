import { config } from '../config';

export const storageUrl = (asset: string): string => {
    if (asset.match(/^http(s)?\:\/\//)) {
      return asset;
    }
    if (config.mock) {
      return asset;
    }
    return config.storageUrl + asset;
};


export function randomIntFromRange(min: number, max: number) {
  return Math.floor(Math.random() * (max - min + 1) + min)
}

export function randomValue(array: any[]) {
  return array[Math.floor(Math.random() * array.length)]
}

export function hexToRgb(hex: string) {
  // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
  const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
  hex = hex.replace(shorthandRegex, (m, r, g, b) => {
    return r + r + g + g + b + b;
  });

  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  } : {r: '', g: '', b: ''};
}

export function distance(x1: number, y1: number, x2: number, y2: number) {
  const xDist = x2 - x1
  const yDist = y2 - y1

  return Math.sqrt(Math.pow(xDist, 2) + Math.pow(yDist, 2))
}

export function shuffleArray(array: any[]) {
  const ar = [...array];
  for (let i = ar.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [ar[i], ar[j]] = [ar[j], ar[i]];
  }
  return ar;
}