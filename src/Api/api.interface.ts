
export interface ApiInterface {
  getAll(model: string): Promise<any>;

  get(model: string, id: number | string): Promise<any>;

  getNextPrev(model: string, id: number | string): Promise<any>;
}