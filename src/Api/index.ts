import { ApiInterface } from './api.interface';
import MockApi from './mock';
import FetchApi from './fetch';
import { config } from '../config';

const resolvers = {
  Fetch: new FetchApi(),
  Mock: new MockApi()
};

const res = config.mock ? resolvers.Mock : resolvers.Fetch;

class ApiClass {
  constructor(private resolver: ApiInterface) {
  }

  public getAll(model: string) {
    return this.resolver.getAll(model);
  }

  public get(model: string, id: number | string) {
    return this.resolver.get(model, id);
  }

  public getNextPrev(model: string, id: number | string) {
    return this.resolver.getNextPrev(model, id);
  }

}

const Api = new ApiClass(res);
export default Api;