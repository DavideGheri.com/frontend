import { ApiInterface } from './api.interface';
import { PortfolioService } from './services/portfolio';
import { Api } from './services/api';
import { config } from '../config';
// import MockApi from './mock';
import { SkillService } from './services/skill';
import { HomepageService } from './services/homepage';

export default class FetchApi implements ApiInterface {
  private portfolioService: PortfolioService;
  private skillService: SkillService;
  private homepageService: HomepageService;
  // private mock: MockApi;
  constructor() {
    const api = new Api(config);
    this.portfolioService = new PortfolioService(api);
    this.skillService = new SkillService(api);
    this.homepageService = new HomepageService(api);
    // this.mock = new MockApi()
  }

  public getAll(model: string): Promise<any> {
    switch (model) {
      case 'portfolio':
        return this.portfolioService.getAll();
      case 'language':
        return this.skillService.getAll();
      case 'homepage':
        return this.homepageService.getAll();
      default:
        return new Promise<any>((res, rej) => {res()});
    }
  }

  public get(model: string, id: string | number): Promise<any> {
    id = parseInt(id + '', 10);
    switch (model) {
      case 'portfolio':
        return this.portfolioService.get(id);
      case 'language':
        return new Promise<any>((res, rej) => {res()});
      default:
        return new Promise<any>((res, rej) => {res()});
    }
  }

  public getNextPrev(model: string, id: string | number): Promise<any> {
    id = parseInt(id + '', 10);
    switch (model) {
      case 'portfolio':
        return this.portfolioService.getNextPrev(id);
      case 'language':
        return new Promise<any>((res, rej) => {rej('Model not found')});
      default:
        return new Promise<any>((res, rej) => {rej('Model not found')});
    }
  }
}