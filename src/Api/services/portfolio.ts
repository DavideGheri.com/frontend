import { PortfolioModel, Portfolios } from '../../models/portfolio';
import { Api } from './api';


export class PortfolioService {
  constructor(private api: Api) {}

  public getAll(): Promise<Portfolios> {
    return fetch(this.api.portfolios.index, {headers: this.api.headers})
      .then(res => res.json());
  }

  public get(id: number): Promise<PortfolioModel> {
    const url = this.api.portfolios.show.replace(':id', id);
    return fetch(url, {headers: this.api.headers})
      .then(res => res.json());
  }

  public getNextPrev(id: number): Promise<{next: PortfolioModel | null, prev: PortfolioModel | null}> {
    const url = this.api.portfolios.nextPrev.replace(':id', id);
    return fetch(url, {headers: this.api.headers})
      .then(res => res.json());
  }
}