import { Skills } from '../../models/skill';
import { Api } from './api';


export class SkillService {
  constructor(private api: Api) {}

  public getAll(): Promise<Skills> {
    return fetch(this.api.skills.index, {headers: this.api.headers})
      .then(res => res.json());
  }
}