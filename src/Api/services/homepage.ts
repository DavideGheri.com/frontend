import { Api } from './api';
import { HomepageModel } from '../../models/homepage';


export class HomepageService {
  constructor(private api: Api) {}

  public getAll(): Promise<HomepageModel> {
    return fetch(this.api.homepage.last, {headers: this.api.headers})
      .then(res => res.json());
  }
}