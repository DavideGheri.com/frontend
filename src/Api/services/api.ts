import { Config } from '../../config';

export class Api {
  private base: string = 'http://api.davidegheri.test/api/';
  private version: string = 'v1';
  private accessToken: string;
  private readonly PORTFOLIOS = 'content-types/2/contents';
  private readonly SKILLS = 'content-types/1/contents';
  private readonly HOMEPAGE = 'content-types/3/contents';

  constructor(config: Config) {
    this.base = config.apiUrl;
    this.version = config.version;
    this.accessToken = config.accessToken;
  }

  get headers(): HeadersInit {
    return {
      'Accept-Language': 'it-IT',
      'Authorization': `Bearer ${this.accessToken}`,
      'Accept': 'application/json'
    }
  }

  private get url(): string {
    return this.base + this.version + '/';
  }

  get portfolios(): any {
    return {
      index: this.url + this.PORTFOLIOS + '?scope=1&paginate=0',
      show: this.url + this.PORTFOLIOS + '/:id',
      nextPrev: this.url + this.PORTFOLIOS + '/:id/next-prev'
    }
  }

  get skills(): any {
    return {
      index: this.url + this.SKILLS + '?scope=1&paginate=0',
    }
  }

  get homepage(): any {
    return {
      last: this.url + this.HOMEPAGE + '/last',
    }
  }
}