import * as faker from 'faker';
import { PortfolioModel, Portfolios } from '../models/portfolio';
import { ApiInterface } from './api.interface';
import { Languages } from '../models/language';

import react from '../assets/images/logos/react.svg';
import angular from '../assets/images/logos/angular.svg';
import html5 from '../assets/images/logos/html5.svg';
import css3 from '../assets/images/logos/css3.svg';
import sass from '../assets/images/logos/sass.svg';
import js from '../assets/images/logos/js.svg';
import php from '../assets/images/logos/php.svg';
import laravel from '../assets/images/logos/laravel.svg';
import redis from '../assets/images/logos/redis.svg';
import mysql from '../assets/images/logos/mysql.svg';
import linux from '../assets/images/logos/linux.svg';
import node from '../assets/images/logos/node.svg';
import nginx from '../assets/images/logos/nginx.svg';

import sw from '../assets/images/sw_art.jpg';

const PORTFOLIOS = 10;

let moccked: Portfolios = [];

const mockPortfolios = (): Portfolios => {
  const portfolios: Portfolios = [];
  if (moccked.length) {
    return moccked;
  }
  for (let i = 1; i <= PORTFOLIOS; i++) {
    portfolios.push({
      id: i,
      title: faker.company.companyName(),
      fields: {
        url: faker.internet.url(),
        content: faker.lorem.words(window['random'](10, 100)),
        featured_image: sw,
      },
      published_at: '2018-05-19'
    })
  }
  moccked = portfolios;
  return portfolios;
};

const clientLangs: Languages = [
  {id: 6, name:'Javascript', logo: js, description: 'Javascript is a client side language bla bla', link: 'https://google.com'},
  {id: 1, name: 'React', logo: react, description: 'React is a Js library to build web components bla bla', link: 'https://google.com'},
  {id: 2, name:'Angular', logo: angular, description: 'Angular 2 is a Js framework to build SPA bla bla', link: 'https://google.com'},
  {id: 3, name:'Html5', logo: html5, description: 'Html 5 is the new web standard bla bla', link: 'https://google.com'},
  {id: 4, name:'Css3', logo: css3, description: 'Css 3 is the new stylesheet standard bla bla', link: 'https://google.com'},
  {id: 5, name:'Scss', logo: sass, description: 'Scss is a css preprocessor for bla bla', link: 'https://google.com'},
];

const serverLangs: Languages = [
  {id: 7, name: 'Php', logo: php, description: 'Php is the most used bla bla', link: 'https://google.com'},
  {id: 8, name: 'Laravel', logo: laravel, description: 'laravel is a Php framework to build modern web applications bla bla', link: 'https://google.com'},
  {id: 9, name: 'Mysql', logo: mysql, description: 'Mysql is the most used relational Database bla bla', link: 'https://google.com'},
  {id: 10, name: 'Node js', logo: node, description: 'Node js is a Javascript bla bla', link: 'https://google.com'},
  {id: 11, name: 'Redis', logo: redis, description: 'Redis is a key value object storage with high performance bla bla', link: 'https://google.com'},
  {id: 12, name: 'Nginx', logo: nginx, description: 'Nginx is a web server bla bla', link: 'https://google.com'},
  {id: 13, name: 'Linux', logo: linux, description: 'Linux is an Os Kernel bla bla', link: 'https://google.com'},
];

const mockLanguages = () => {
  return {
    client: clientLangs,
    server: serverLangs
  }
};

export default class MockApi implements ApiInterface {
  public getAll(model: string): Promise<any> {
    return new Promise((res, rej) => {
      switch (model) {
        case 'portfolio':
          res(mockPortfolios());
          break;
        case 'language':
          res(mockLanguages());
        default:
          rej('Model not found');
      }
    })
  }

  public get(model: string, id: string | number) {
    id = parseInt(id + '', 10);
    return new Promise((res, rej) => {
      switch (model) {
        case 'portfolio':
          const portfolios = mockPortfolios();
          const portfolio = portfolios.find((p: PortfolioModel) => p.id === id);
          if (portfolio) {
            setTimeout(() => res(portfolio), 1000);
            // res(portfolio);
          } else {
            rej('Model not found');
          }
          break;
        default:
          rej('Model not found');
      }
    })
  }

  public getNextPrev(model: string, id: string | number) {
    id = parseInt(id + '', 10);
    return new Promise((res, rej) => {
      switch (model) {
        case 'portfolio':
          const portfolios = mockPortfolios();
          const portfolio = portfolios.find((p: PortfolioModel) => p.id === id);
          if (portfolio) {
            const current = portfolios.findIndex((p: PortfolioModel) => p.id === portfolio.id);
            const next = portfolios[current + 1] || null;
            const prev = portfolios[current - 1] || null;
            res({next, prev});
          } else {
            rej('Model not found');
          }
          break;
        default:
          rej('Model not found');
      }
    })
  }

}