import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ActionCreators from '../../../actions';
import { GlobalProps, Scroll } from '../../../models/states';
import * as classNames from 'classnames';
import { Socials } from '../Socials';

interface Props extends GlobalProps {
  scroll: Scroll
}

class NavbarComponent extends React.Component<Props, {}> {
  constructor(props: any) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  public shouldComponentUpdate(props: any, state: any): boolean {
    return this.props.scroll.scrolled !== props.scroll.scrolled;
  }

  public render(): JSX.Element {
    const { scrolled } = this.props.scroll;

    const classes = classNames({
      navbar: true,
      'bg-white': false, // TODO theme,
      'text-white': true,
      'scrolled': scrolled,
      'bg-indigo-darker': scrolled,
    });

    return (
      <header>
        <nav className={classes}>
          <div className="navbar-brand">
            <svg className="navbar-logo" width="54" height="54" viewBox="0 0 54 54"
                 xmlns="http://www.w3.org/2000/svg">
              <path
                d="M13.5 22.1c1.8-7.2 6.3-10.8 13.5-10.8 10.8 0 12.15 8.1 17.55 9.45 3.6.9 6.75-.45 9.45-4.05-1.8 7.2-6.3 10.8-13.5 10.8-10.8 0-12.15-8.1-17.55-9.45-3.6-.9-6.75.45-9.45 4.05zM0 38.3c1.8-7.2 6.3-10.8 13.5-10.8 10.8 0 12.15 8.1 17.55 9.45 3.6.9 6.75-.45 9.45-4.05-1.8 7.2-6.3 10.8-13.5 10.8-10.8 0-12.15-8.1-17.55-9.45-3.6-.9-6.75.45-9.45 4.05z"/>
            </svg>
            <span className="navbar-title"><a href="/" onClick={this.handleClick}>Davide Gheri</a></span>
          </div>
          <Socials/>
        </nav>
      </header>
    )
  }

  private handleClick(e: any) {
    e.preventDefault();
    this.props.push('/');
  }
}

const mapStateToProps = (state: any) => ({
  scroll: state.scroll,
});

const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators(ActionCreators, dispatch)
);

export const Navbar = connect(mapStateToProps, mapDispatchToProps)(NavbarComponent);