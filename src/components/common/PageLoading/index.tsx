import * as React from 'react';
import * as classNames from 'classnames';
import { CanvasLoading } from './CanvasLoading';

interface PageLoadingProps {
  section?: boolean;
  background?: string;
}

const canvas = false;

export const PageLoading = (props: PageLoadingProps) => {
  const classes = classNames({
    'page-loading': true,
    'section': props.section || false
  });
  if (canvas) {
    return (
      <div className={classes}><CanvasLoading background={props.background}/></div>
    )
  }
  return (
    <div className={classes}>
      <svg width="140px" height="140px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"
           preserveAspectRatio="xMidYMid" className="lds-ripple">
        <circle cx="50" cy="50" r="17.2164" fill="none" stroke="#015464" strokeWidth="4">
          <animate attributeName="r" calcMode="spline" values="0;40" keyTimes="0;1" dur="1.8" keySplines="0 0.2 0.8 1"
                   begin="-0.9s" repeatCount="indefinite"/>
          <animate attributeName="opacity" calcMode="spline" values="1;0" keyTimes="0;1" dur="1.8" keySplines="0.2 0 0.8 1"
                   begin="-0.9s" repeatCount="indefinite"/>
        </circle>
        <circle cx="50" cy="50" r="35.6916" fill="none" stroke="#00a1b3" strokeWidth="4">
          <animate attributeName="r" calcMode="spline" values="0;40" keyTimes="0;1" dur="1.8" keySplines="0 0.2 0.8 1"
                   begin="0s" repeatCount="indefinite"/>
          <animate attributeName="opacity" calcMode="spline" values="1;0" keyTimes="0;1" dur="1.8" keySplines="0.2 0 0.8 1"
                   begin="0s" repeatCount="indefinite"/>
        </circle>
      </svg>
    </div>
  );
};
