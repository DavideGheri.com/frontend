import { randomIntFromRange } from '../../../utilities';

export class Particle {
  public x: number;
  public y: number;
  public radius: number;
  public color: string;
  public radians: number;
  public velocity: number = 0.05;
  public distance: number;
  public lastMouse: {x: number, y: number};
  public mouse: {x: number, y: number};
  private readonly context: CanvasRenderingContext2D;

  constructor(x: number, y: number, radius: number, color: string, context: CanvasRenderingContext2D, mouse: {x: number, y: number}) {
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.color = color;
    this.context = context;
    this.radians = Math.random() * Math.PI * 2;
    this.velocity = 0.05;
    this.distance = randomIntFromRange(20, 70);
    this.lastMouse = {x, y};
    this.mouse = mouse;
  }

  public update() {
    const last = {x: this.x, y: this.y};
    this.radians += this.velocity;
    // this.lastMouse.x = (this.mouse.x - this.lastMouse.x) * 0.05;
    // this.lastMouse.y = (this.mouse.y - this.lastMouse.y) * 0.05;
    this.x = this.lastMouse.x + Math.cos(this.radians) * this.distance;
    this.y = this.lastMouse.y + Math.sin(this.radians) * this.distance;

    this.draw(last);
  }

  private draw(last: {x: number, y: number}) {
    const c = this.context;
    c.beginPath();
    c.strokeStyle = this.color;
    c.lineWidth = this.radius;
    c.moveTo(last.x, last.y);
    c.lineTo(this.x, this.y);
    c.stroke();
    c.closePath();
  }

}