import * as React from 'react';
import { RefObject } from 'react';
import { Particle } from './Particle';
import { randomIntFromRange, randomValue, hexToRgb } from '../../../utilities';

const colors = ['#E6F5F7', '#9FE3DD', '#51BFB5','#38a89d', '#2f365f'];

export class CanvasLoading extends React.Component<{background?: string}, any> {
  private canvasRef: RefObject<HTMLCanvasElement> = React.createRef<HTMLCanvasElement>();
  private canvas: HTMLCanvasElement;
  private canvasContext: CanvasRenderingContext2D;
  private particles: Particle[];
  private particlesNumer: number = 20;
  private mouse: {x: number, y: number};

  public componentDidMount() {
    this.setup();
  }

  public shouldComponentUpdate() {
    return false;
  }

  public render() {
    return <canvas ref={this.canvasRef}/>
  }

  private setup() {
    this.canvas = this.canvasRef.current as HTMLCanvasElement;
    this.canvasContext = this.canvas.getContext('2d') as CanvasRenderingContext2D;
    const parent = this.canvas.parentNode as HTMLElement;

    this.canvas.width = parent.clientWidth;
    this.canvas.height = parent.clientHeight;
    this.mouse = {
      x: this.canvas.width / 2,
      y: this.canvas.height / 2
    };
    // window.addEventListener('mousemove', (e: any) => {
    //   this.mouse.x = e.pageX - this.canvas.offsetLeft;
    //   this.mouse.y = e.pageY - this.canvas.offsetTop;
    // });

    this.init();
    this.animate();
  }

  private init() {
    this.particles = [];
    for (let i = 0; i < this.particlesNumer; i++) {
      const radius = randomIntFromRange(1, 2);
      this.particles.push(new Particle(this.canvas.width / 2, this.canvas.height / 2, radius, randomValue(colors), this.canvasContext, this.mouse));
    }
  }

  private animate() {
    window.requestAnimationFrame(this.animate.bind(this));
    if (this.props.background) {
      const rgb = hexToRgb(this.props.background);
      this.canvasContext.fillStyle = `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, 0.05)`;
    } else {
      this.canvasContext.fillStyle = 'rgba(232, 255, 254, 0.05)';
    }
    this.canvasContext.fillRect(0, 0, this.canvas.width, this.canvas.height);
    this.particles.forEach(p => p.update());
  }
}