import * as React from 'react';
import { connect } from 'react-redux';
import { GlobalProps, GlobalState } from '../../../models/states';
import { bindActionCreators, Dispatch } from 'redux';
import * as ActionCreators from '../../../actions';
import { PortfolioModel } from '../../../models/portfolio';
import { Arrow } from '../Arrow';
import { Link } from 'react-router-dom';
import { FeaturedImage } from '../FeaturedImage';

interface Props extends GlobalProps {
  portfolio: PortfolioModel;
  prev: PortfolioModel | null;
  next: PortfolioModel | null;
}

interface State {
  current: PortfolioModel;
  next: PortfolioModel;
  prev: PortfolioModel;
}

class NextPrevComponent extends React.Component<Props, State> {
  constructor(props: any) {
    super(props);
  }

  public componentWillMount() {
    this.setState({current: this.props.portfolio});
    this.props.getNextPrevPortfolio(this.props.portfolio.id);
  }

  public componentWillReceiveProps(props: Props) {
    if (this.state.current.id !== props.portfolio.id) {
      this.setState({current: props.portfolio});
      this.props.getNextPrevPortfolio(props.portfolio.id);
    }
  }

  public render() {
    const { next, prev } = this.props;
    let nextSrc; let prevSrc;
    if (next) {
      nextSrc = next.fields.featured_image;
    }
    if (prev) {
      prevSrc = prev.fields.featured_image;
    }
    return (
      <section className="section flex flex-wrap bg-grey-light p-3">
        <div className="w-full md:w-1/2">
          {prev &&
          <Link to={`/portfolio/${prev.id}`}>
            <div className="next-prev prev">
              <div className="content-wrapper">
                {prevSrc && <FeaturedImage image={prevSrc} title={prev.title}/>}
                <div className="content">
                  <div className="arrow"><Arrow/></div>
                  <h4>{prev.title}</h4>
                </div>
              </div>
            </div>
          </Link>
          }
        </div>
        <div className="w-full md:w-1/2">
          {next &&
          <Link to={`/portfolio/${next.id}`}>
            <div className="next-prev next">
              <div className="content-wrapper">
                {nextSrc && <FeaturedImage image={nextSrc} title={next.title}/>}
                <div className="content">
                  <h4>{next.title}</h4>
                  <div className="arrow"><Arrow/></div>
                </div>
              </div>
            </div>
          </Link>
          }
        </div>
      </section>
    )
  }
}

const mapStateToProps = (state: GlobalState, ownState: {portfolio: PortfolioModel}) => ({
  next: state.portfolio.next,
  prev: state.portfolio.prev,
  portfolio: ownState.portfolio
});

const mapDispatchToProps = (dispatch: Dispatch) => (
  bindActionCreators(ActionCreators, dispatch)
);

export const NextPrev = connect(mapStateToProps, mapDispatchToProps)(NextPrevComponent);