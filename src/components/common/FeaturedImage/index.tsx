import * as React from 'react';
import { FeaturedImage as IFeaturedImage } from '../../../models/featuredImage';
import { storageUrl } from '../../../utilities';
import { RefObject } from 'react';

interface FeaturedImageProps {
  image: IFeaturedImage;
  classNames?: string | null;
  title: string;
}

// Math.ceil(this.getBoundingClientRect().width/window.innerWidth*100)+'vw'

export class FeaturedImage extends React.Component<FeaturedImageProps, any> {
  private imageRef: RefObject<HTMLImageElement> = React.createRef<HTMLImageElement>();
  private image: HTMLImageElement;

  constructor(props: FeaturedImageProps) {
    super(props);
    this.state = {
      sizes: '1px'
    };
    this.onLoad = this.onLoad.bind(this);
  }

  public componentDidMount() {
    this.setState({sizes: this.props.image.sizes});
    this.image = this.imageRef.current as HTMLImageElement;
  }

  public onLoad() {
    const sizes = Math.ceil(this.image.getBoundingClientRect().width / window.innerWidth*100)+'vw';
    this.setState({sizes})
  }

  public render() {
    const { image, classNames, title } = this.props;
    const { sizes } = this.state;
    const src = storageUrl(image.full_url);
    return (
      <img src={src} ref={this.imageRef}
           onLoad={this.onLoad}
           srcSet={image.srcSet}
           sizes={sizes}
           alt={title}
           width={image.width}
           className={classNames || ''}/>
    )
  }
}