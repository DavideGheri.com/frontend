import * as React from 'react';

export const Footer = () => (
  <footer className="footer">
    <p>DavideGheri.com | Made with &hearts; by Davide Gheri</p>
  </footer>
);