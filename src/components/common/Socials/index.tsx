import * as React from 'react';
import InlineSVG from 'react-svg-inline';
import facebook from '../../../assets/images/socials/facebook.svg';
import linkedin from '../../../assets/images/socials/linkedin.svg';


export const Socials = (props: any) => {
  const socials = [
    {
      icon: facebook,
      url: 'https://www.facebook.com/Davide.Gheri'
    },
    {
      icon: linkedin,
      url: 'https://www.linkedin.com/in/davide-gheri/'
    }
  ];
  return (
    <ul className="socials">
      {socials.map((social, i) => (
        <li key={i}><a href={social.url}><InlineSVG svg={social.icon} style={{width: '100%'}}/></a></li>
      ))}
    </ul>
  )
};