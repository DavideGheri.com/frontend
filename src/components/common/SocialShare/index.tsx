import * as React from 'react';
import InlineSVG from 'react-svg-inline';
import { PortfolioModel } from '../../../models/portfolio';
import * as querystring from 'querystring';
import * as classNames from 'classnames';

import facebook from '../../../assets/images/socials/facebook.svg';
import twitter from '../../../assets/images/socials/twitter.svg';

interface Social {
  title: string;
  url: string;
  icon: string;
  query: {
    url: string;
    text?: string;
  }
}

interface SocialShareState {
  // [name: string]: Social;
  socials: Social[];
  visible: boolean;
}

interface SocialShareProps {
  portfolio: PortfolioModel;
}

export class SocialShare extends React.Component<SocialShareProps, SocialShareState> {
  constructor(props: any) {
    super(props);
    this.state = {
      visible: false,
      socials: [
        {
          url: 'www.facebook.com/sharer.php',
          title: 'Facebook',
          icon: facebook,
          query: {
            url: 'u'
          }
        },
        {
          url: 'twitter.com/intent/tweet',
          title: 'Twitter',
          icon: twitter,
          query: {
            url: 'url',
            text: 'text'
          }
        }
      ]
    }
    this.onScroll = this.onScroll.bind(this);
  }

  public componentDidMount() {
    window.addEventListener('scroll', this.onScroll);
  }

  public componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll);
  }

  public onScroll(e: Event) {
    const document = e.target as Document;
    const scroll = document.documentElement.scrollTop;
    const title = document.getElementById('portfolio-title');
    if (title) {
      this.setState({visible: title.getBoundingClientRect().top <= scroll});
    }
  }

  public buildurl(social: Social): string {
    const { portfolio } = this.props;
    const title = portfolio.title;
    const url = window.location.origin + '/portfolio/' + portfolio.id;
    const fields = social.query;
    const query = {
      url
    };
    if (fields.text) {
      query[fields.text] = title;
    }
    return `https://${social.url}?${querystring.stringify(query)}`;
  }

  public render() {
    const classes = classNames({
      'social-share': true,
      'visible': this.state.visible
    });
    return(
      <ul className={classes}>
        {this.state.socials.map((social, i) => (
          <li key={i}><a href={this.buildurl(social)} target="_blank">
            <InlineSVG svg={social.icon} style={{width: '100%'}}/>
          </a></li>
        ))}
      </ul>
    )
  }
}