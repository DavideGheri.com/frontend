import * as React from 'react';
import { withRouter } from 'react-router';
import { History, Location } from 'history';

interface Props  {
  children: any;
  history: History;
  location: Location;
  match: any;
}

class ScrollTopComponent extends React.Component<Props> {
  constructor(props: any) {
    super(props);
  }

  public componentDidUpdate(props: Props) {
    if (props.location.key !== this.props.location.key) {
      window.scrollTo(0,0);
    }
  }

  public render() {
    return this.props.children;
  }
}

export const ScrollTop = withRouter(ScrollTopComponent);