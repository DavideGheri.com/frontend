import * as React from 'react';

interface TagProps {
  name: string;
}

export const Tag = (props: TagProps) => (
  <span className="inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker mr-2">#{props.name}</span>
);