import * as React from 'react';
import { connect } from 'react-redux';
import { GlobalState, PageProps } from '../../models/states';
import { Dispatch } from 'redux';
import { bindActionCreators } from 'redux';
import * as ActionCreators from '../../actions';
import { PortfolioModel } from '../../models/portfolio';
import { NextPrev } from '../common/NextPrev';
import { PageLoading } from '../common/PageLoading';
import Helmet from 'react-helmet';
import { Tag } from '../common/Tag';
import { SocialShare } from '../common/SocialShare';
import { FeaturedImage } from '../common/FeaturedImage';
import { FeaturedImage as IFeaturedImage } from '../../models/featuredImage';

interface Props extends PageProps {
  portfolio: PortfolioModel | null;
}

interface State {
  id?: number;
  portfolio?: PortfolioModel | null;
}

class PortfolioPage extends React.Component<Props, State> {

  constructor(props: any) {
    super(props);
    this.state = {};
  }

  public componentWillMount() {
    const { id } = this.props.match.params;
    if (id) {
      this.setState({id});
      this.props.getPortfolio(id)
    }
  }

  public componentWillReceiveProps(props: Props) {
    const { id } = props.match.params;
    if (id !== this.state.id) {
      this.setState({id, portfolio: null});
      this.props.getPortfolio(id);
    } else {
      this.setState({portfolio: props.portfolio})
    }
  }

  public render() {
    const { portfolio } = this.state;

    let featuredImage: IFeaturedImage | null;
    if (portfolio && portfolio.fields.featured_image) {
      featuredImage = portfolio.fields.featured_image;
    } else {
      featuredImage = null;
    }

    if (portfolio) {
      return (
        <main className="portfolio-page">
          <Helmet>
            <title>{portfolio.title}</title>
          </Helmet>
          {portfolio.url &&
          <a href={portfolio.url} target="_blank" className="featured-wrapper">
            <section className="featured">
              <figure>
                {featuredImage && <FeaturedImage image={featuredImage} title={portfolio.title} />}
              </figure>
              <h1 id="portfolio-title">{portfolio.title}</h1>
            </section>
          </a>
          }
          {!portfolio.url &&
          <section className="featured">
            <figure>
              {featuredImage && <FeaturedImage image={featuredImage} title={portfolio.title} />}
            </figure>
            <h1 id="portfolio-title">{portfolio.title}</h1>
          </section>
          }
          <section className="main py-6">
            <SocialShare portfolio={portfolio}/>
            <article className="w-full md:w-3/5 ml-auto mr-auto px-6 md:px-0">
              {portfolio.tags &&
              portfolio.tags.map(tag => (
                <Tag key={tag.id} name={tag.name}/>
              ))
              }
              {/*<h1>{portfolio.title}</h1>*/}
              <div className="content" id="content-wrapper">
                <div dangerouslySetInnerHTML={{__html: portfolio.fields.content}}/>
              </div>
            </article>
          </section>
          <NextPrev portfolio={portfolio}/>
        </main>
      )
    }
    return <PageLoading/>
  }
}


const mapStateToProps = (state: GlobalState) => ({
  portfolio: state.portfolio.active
});

const mapDispatchToProps = (dispatch: Dispatch) => (
  bindActionCreators(ActionCreators, dispatch)
);

export const Portfolio = connect(mapStateToProps, mapDispatchToProps)(PortfolioPage);