// import * as React from 'react';
// import { DanteEditor} from 'Dante2/es/index';
// // import DanteImagePopover from 'Dante2/es/components/popovers/image'
// // import DanteAnchorPopover from 'Dante2/es/components/popovers/link'
// // import DanteInlineTooltip from 'Dante2/es/components/popovers/addButton'
// // import DanteTooltip from 'Dante2/es/components/popovers/toolTip'
// import ImageBlock from 'Dante2/es/components/blocks/image'
// // import EmbedBlock from 'Dante2/es/components/blocks/embed'
// // import VideoBlock from 'Dante2/es/components/blocks/video'
// // import PlaceholderBlock from 'Dante2/es/components/blocks/placeholder'
// // import { resetBlockWithType, addNewBlockAt } from 'Dante2/es/model/index'
// import { addNewBlockAt } from 'Dante2/es/model/index'
//
// export class Content extends React.Component<{content: any}, any> {
//
//   public defaultOptions(options: any) {
//     // default options
//     if (options == null) {
//       options = {}
//     }
//     const defaultOptions: any = {};
//     defaultOptions.el = 'app';
//     defaultOptions.content = "";
//     defaultOptions.read_only = false;
//     defaultOptions.spellcheck = false;
//     defaultOptions.widgets = [
//       {
//         title: 'add an image',
//         icon: 'image',
//         type: 'image',
//         block: ImageBlock,
//         editable: true,
//         renderable: true,
//         breakOnContinuous: true,
//         wrapper_class: "graf graf--figure",
//         selected_class: "is-selected is-mediaFocused",
//         selectedFn: (block: any) => {
//           const { direction } = block.getData().toJS()
//           switch (direction) {
//             case "left":
//               return "graf--layoutOutsetLeft";
//             case "center":
//               return ""
//             case "wide":
//               return "sectionLayout--fullWidth";
//             case "fill":
//               return "graf--layoutFillWidth";
//             default:
//               return "";
//           }
//         },
//         handleEnterWithoutText(ctx: any, block: any) {
//           const { editorState } = ctx.state
//           return ctx.onChange(addNewBlockAt(editorState, block.getKey()))
//         },
//         handleEnterWithText(ctx: any, block: any) {
//           const { editorState } = ctx.state
//           return ctx.onChange(addNewBlockAt(editorState, block.getKey()))
//         },
//         widget_options: {
//           displayOnInlineTooltip: true,
//           insertion: "upload",
//           insert_block: "image"
//         },
//         options: {
//           upload_url: options.upload_url,
//           upload_headers: options.upload_headers,
//           upload_formName: options.upload_formName,
//           upload_callback: options.image_upload_callback,
//           image_delete_callback: options.image_delete_callback,
//           image_caption_placeholder: options.image_caption_placeholder
//         }
//       }
//     ];
//     defaultOptions.tooltips = [];
//
//     defaultOptions.data_storage = {
//       url: null,
//       method: "POST",
//       success_handler: null,
//       failure_handler: null,
//       interval: 1500
//     }
//
//     defaultOptions.default_wrappers = [
//       { className: 'graf--p', block: 'unstyled' },
//       { className: 'graf--h2', block: 'header-one' },
//       { className: 'graf--h3', block: 'header-two' },
//       { className: 'graf--h4', block: 'header-three' },
//       { className: 'graf--blockquote', block: 'blockquote' },
//       { className: 'graf--insertunorderedlist', block: 'unordered-list-item' },
//       { className: 'graf--insertorderedlist', block: 'ordered-list-item' },
//       { className: 'graf--code', block: 'code-block' },
//       { className: 'graf--bold', block: 'BOLD' },
//       { className: 'graf--italic', block: 'ITALIC' }
//     ];
//
//     defaultOptions.continuousBlocks = [
//       "unstyled",
//       "blockquote",
//       "ordered-list",
//       "unordered-list",
//       "unordered-list-item",
//       "ordered-list-item",
//       "code-block"
//     ];
//
//     defaultOptions.character_convert_mapping = {
//       '> ': "blockquote",
//       '*.': "unordered-list-item",
//       '* ': "unordered-list-item",
//       '- ': "unordered-list-item",
//       '1.': "ordered-list-item",
//       '# ': 'header-one',
//       '##': 'header-two',
//       '==': "unstyled",
//       '` ': "code-block"
//     };
//
//     return defaultOptions
//   }
//
//   public render() {
//     try {
//       const content = JSON.parse(this.props.content);
//       console.log(content)
//       return (
//         <DanteEditor
//           config={this.defaultOptions({})}
//           content={content}
//         />
//       )
//     } catch (e) {
//       return null;
//     }
//   }
// }