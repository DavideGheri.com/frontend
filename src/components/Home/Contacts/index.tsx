import * as React from 'react';
import { GlobalState } from '../../../models/states';
import { bindActionCreators, Dispatch } from 'redux';
import * as ActionCreators from '../../../actions';
import { connect } from 'react-redux';
import { Stars } from '../Fullpage/Stars';

class ContactsComponent extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
  }

  public render() {
    return (
      <section className="p-3 text-white relative z-10">
        <div className="footer-stars"><Stars/></div>
        <h2 className="section-title">Contacts</h2>
        <div className="flex flex-wrap">
          <div className="w-full md:w-1/2">
            {this.props.image && <img src={this.props.image}/>}
          </div>
          <div className="w-full md:w-1/2">
            <div className="p-6 leading-normal" dangerouslySetInnerHTML={{__html: this.props.content}}/>
          </div>
        </div>
      </section>
    )
  }
}

const mapStateToProps = (state: GlobalState) => ({
  content: state.homepage.content ? state.homepage.content.fields['contacts'] : null,
  image: state.homepage.content ? state.homepage.content.fields['contacts_image'] : null,
});

const mapDispatchToProps = (dispatch: Dispatch) => (
  bindActionCreators(ActionCreators, dispatch)
);

export const Contacts = connect(mapStateToProps, mapDispatchToProps)(ContactsComponent);