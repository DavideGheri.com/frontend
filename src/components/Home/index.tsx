import * as React from 'react';
import Helmet from 'react-helmet';
import { Fullpage } from './Fullpage';
import { About } from './About';
import { Portfolio } from './Portfolio';
import { Languages } from './Languages';
import { Contacts } from './Contacts';

export const Home = (props: any) => {
  return (
    <div>
      <Helmet>
        {/*<title>Home</title>*/}
        <meta name="description" content="full stack web develoer based in Florence, IT"/>
        <meta property="og:type" content="page"/>
        <meta property="og:description" content="full stack web develoer based in Florence, IT"/>
        <script type="application/ld+json">{`
          {
            "@context": "http://schema.org"
          }
        `}</script>
      </Helmet>
      <Fullpage/>
      <About/>
      <Portfolio/>
      <Languages/>
      <Contacts/>
    </div>
  )
};