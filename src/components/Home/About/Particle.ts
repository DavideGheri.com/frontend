import { distance } from '../../../utilities';
import { resolveCollision } from '../../../utilities/collision-detection';


export class Particle {
  public x: number;
  public y: number;
  public radius: number;
  public color: string;
  public context: CanvasRenderingContext2D;
  public canvas: HTMLCanvasElement;
  public velocity = {
    x: Math.random() - 0.05,
    y: Math.random() - 0.05
  };
  public mass: number = 1;
  public opacity: number = 0;
  public mouse: {x: number, y: number}

  constructor(x: number, y: number, radius: number, color: string, context: CanvasRenderingContext2D, canvas: HTMLCanvasElement, mouse: {x: number, y: number}) {
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.color = color;
    this.context = context;
    this.canvas = canvas;
    this.mouse = mouse;
  }

  public update(particles: Particle[]) {
    this.draw();
    particles.forEach(p => {
      if (p === this) {
        return;
      }
      if (distance(this.x, this.y, p.x, p.y) - this.radius * 2 < 0) {
        resolveCollision(this, p);
      }
    });
    if (this.x - this.radius <= 0 || this.x + this.radius >= this.canvas.width + 100) {
      this.velocity.x = -this.velocity.x;
    }
    if (this.y - this.radius <= -100 || this.y + this.radius >= this.canvas.height + 100) {
      this.velocity.y = -this.velocity.y;
    }
    if (distance(this.mouse.x, this.mouse.y, this.x, this.y) < 40) {
      this.opacity += 0.2;
      this.opacity = Math.min(this.opacity, 0.4);
    } else if (this.opacity > 0) {
      this.opacity -= 0.2;
      this.opacity = Math.max(0, this.opacity);
    }
    this.x += this.velocity.x;
    this.y += this.velocity.y;
  }

  public draw() {
    const c = this.context;
    c.beginPath();
    c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
    c.save();
    c.globalAlpha = this.opacity;
    c.fillStyle = this.color;
    c.fill();
    c.restore();
    c.strokeStyle = this.color;
    c.stroke();
    c.closePath();
  }
}