import * as React from 'react';
import * as ActionCreators from '../../../actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { CollisionCanvas } from './CollisionCanvas';
import { GlobalState } from '../../../models/states';

class AboutComponent extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      showCanvas: true,
    };
    this.onResize = this.onResize.bind(this);
  }

  public componentDidMount() {
    this.onResize();
    window.addEventListener('resize', this.onResize);

    this.props.getHomepageContent();
  }

  public componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
  }

  public onResize() {
    this.setState({showCanvas: window.innerWidth > 992});
  }

  public render() {
    const { showCanvas } = this.state;
    return (
      <section className="bg-teal flex flex-wrap relative about-section">
        <div className="w-full md:w-3/4 about-quote p-6 pl-10">
          <div>
            <h4 className="vertical text-indigo-darker">WhoAmI</h4>
            <h2 className="pb-2 text-3xl">Davide Gheri</h2>
          </div>
          <p dangerouslySetInnerHTML={{__html: this.props.content}}/>
        </div>
        {showCanvas && <div className="w-full md:w-1/2 collision-canvas">
          <CollisionCanvas/>
        </div>}
      </section>
    )
  }
}

const mapStateToProps = (state: GlobalState) => ({
  content: state.homepage.content ? state.homepage.content.fields['who-am-i'] : null
});

const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators(ActionCreators, dispatch)
);

export const About = connect(mapStateToProps, mapDispatchToProps)(AboutComponent);