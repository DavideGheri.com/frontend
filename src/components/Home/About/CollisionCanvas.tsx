import * as React from 'react';
import { RefObject } from 'react';
import { Particle } from './Particle';
import {
  distance,
  randomIntFromRange, randomValue } from '../../../utilities';

export class CollisionCanvas extends React.Component {
  public colors = ['#2185C5', '#7ECEFD', '#FFF6E5', '#FF7F66'];
  private canvasRef: RefObject<HTMLCanvasElement> = React.createRef<HTMLCanvasElement>();
  private canvas: HTMLCanvasElement;
  private canvasContext: CanvasRenderingContext2D;
  private particles: Particle[] = [];
  private particlesNumber: number = 100;
  private mouse: {
    x: number,
    y: number
  };

  constructor(props: any) {
    super(props);
    this.onResize = this.onResize.bind(this);
    this.onMouseMove = this.onMouseMove.bind(this);
  }

  public componentDidMount() {
    this.setup();
    window.addEventListener('resize', this.onResize);
    this.animate();
    window.addEventListener('mousemove', this.onMouseMove);
  }

  public onResize() {
    this.setup();
  }

  public onMouseMove(e: MouseEvent) {
    if (this.canvas) {
      const rect = this.canvas.getBoundingClientRect() as DOMRect;
      this.mouse.x = e.pageX - rect.left;
      this.mouse.y = e.pageY - rect.top;
    }
  }

  public componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
    window.removeEventListener('mousemove', this.onMouseMove);
  }

  public shouldComponentUpdate() {
    return false;
  }

  public render() {
    return <canvas ref={this.canvasRef}/>
  }

  public animate() {
    window.requestAnimationFrame(this.animate.bind(this));
    if (this.canvas && this.canvasContext) {
      this.canvasContext.clearRect(0, 0, this.canvas.width, this.canvas.height);
      this.particles.forEach(p => p.update(this.particles))
    }
  }

  private setup() {
    this.canvas = this.canvasRef.current as HTMLCanvasElement;
    if (!this.canvas) {
      return;
    }
    this.canvasContext = this.canvas.getContext('2d') as CanvasRenderingContext2D;
    if (!this.canvasContext) {
      return;
    }
    const parent = this.canvas.parentNode as HTMLElement;

    this.canvas.width = parent.clientWidth;
    this.canvas.height = parent.clientHeight;
    this.mouse = {
      x: this.canvas.width / 2,
      y: this.canvas.height / 2
    };
    this.init();
  }

  private init() {
    this.particles = [];
    const radius = 10;
    for (let i = 0; i < this.particlesNumber; i++) {
      let x = randomIntFromRange(radius, this.canvas.width - radius);
      let y = randomIntFromRange(radius, this.canvas.height - radius);
      if (i > 0) {
        for (let j = 0; j < this.particles.length; j++) {
          if (distance(x, y, this.particles[j].x, this.particles[j].y) - radius * 2 < 0) {
            x = randomIntFromRange(radius, this.canvas.width - radius);
            y = randomIntFromRange(radius, this.canvas.height - radius);
            j = -1;
          }
        }
      }
      this.particles.push(new Particle(x, y, radius, randomValue(this.colors), this.canvasContext, this.canvas, this.mouse))
    }
  }

}