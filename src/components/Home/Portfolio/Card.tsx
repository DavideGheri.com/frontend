import * as React from 'react';
import { PortfolioModel } from '../../../models/portfolio';
import * as classNames from 'classnames';
import { Link } from 'react-router-dom';
import { Tag } from '../../common/Tag';
import { FeaturedImage } from '../../common/FeaturedImage';
import { FeaturedImage as IFeaturedImage } from '../../../models/featuredImage';

interface CardProps extends PortfolioModel {
  scrolled: boolean;
}

export const Card = (props: CardProps) => {
  const counter = Math.floor(Math.random() * (2 - 1 + 1)) + 1;
  const classes = classNames({
    'item': true,
    'max-w-xs': true,
    'm-3': true,
    'card': true,
    'visible': props.scrolled,
    [`grid-row-${counter}`]: true,
    [`item-${props.id}`]: true,
  });
  const style = {animationDelay: '0.' + props.id + 's'};

  let featuredImage: IFeaturedImage | null;
  if (props.fields.featured_image) {
    featuredImage = props.fields.featured_image;
  } else {
    featuredImage = null;
  }

  return (
    <div className={classes} style={style}>
        <div className="content">
          {featuredImage && <FeaturedImage image={featuredImage} title={props.title} classNames="w-full"/>}
          <div className="px-6 py-4">
            <div className="font-bold text-xl mb-2"><Link to={`portfolio/${props.id}`}>{props.title}</Link></div>
            <p className="text-grey-darker text-base leading-normal">
              {props.fields.excerpt || props.fields.content}
            </p>
          </div>
          <div className="px-6 py-4">
            {props.tags &&
            props.tags.map(tag => (
              <Tag key={tag.id} name={tag.name}/>
            ))
            }
          </div>
        </div>
    </div>
  );
};