import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ActionCreators from '../../../actions';
import { Portfolios, PortfolioModel } from '../../../models/portfolio';
import { Card } from './Card';
import { PageLoading } from '../../common/PageLoading';
import { GlobalProps } from '../../../models/states';
import * as classNames from 'classnames';

function resizeGridItem(item: any){
  const grid = document.getElementsByClassName("grid-container")[0];
  const rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'), 10);
  const rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'), 10);
  let rowSpan = Math.ceil((item.querySelector('.content').getBoundingClientRect().height+rowGap)/(rowHeight+rowGap));
  if (rowSpan < 7) {
    rowSpan = 7;
  }
  item.style.gridRowEnd = "span "+rowSpan;
}

function resizeAllGridItems(){
  const allItems = document.getElementsByClassName("item");
  setTimeout(() => {
    for (const item of allItems as any) {
      resizeGridItem(item)
    }
  }, 10);
}

interface Props extends GlobalProps {
  scrolled: boolean;
  portfolios: Portfolios;
}

interface State {
  portfolios: Portfolios,
  alreadyScrolled: boolean;
}

class PortfolioComponent extends React.Component<Props, State> {
  constructor(props: any) {
    super(props);
    this.state = {
      portfolios: [] as Portfolios,
      alreadyScrolled: false,
    }
  }

  public componentWillMount() {
    this.props.getPortfolios();
  }

  public componentWillUnmount() {
    window.removeEventListener('resize', resizeAllGridItems)
  }

  public componentWillReceiveProps(props: Props) {
    if (props.scrolled && !this.props.scrolled) {
      // this.props['getPortfolios']();
      this.setState({alreadyScrolled: true});
    }
    this.setState({portfolios: props.portfolios || []});
  }

  public componentDidUpdate(props: any, state: any) {
    if (!props.portfolios.length && this.props.portfolios.length) {
      window.addEventListener('resize', resizeAllGridItems);
    }
    resizeAllGridItems();
  }

  public renderPortfolioList() {
    // const { scrolled } = this.props;
    const { portfolios, alreadyScrolled } = this.state;
    if (portfolios.length) {
      return (
        <div className="grid-container flex flex-wrap justify-center grid">
          {this.state.portfolios.map((portfolio: PortfolioModel) => (
            <Card {...portfolio} scrolled={alreadyScrolled} key={portfolio.id}/>
          ))}
        </div>
      )
    } else {
      return <PageLoading section={true} background="#2f365f"/>
    }
  }

  public render() {
    const classes = classNames({
      'px-6': false,
      'pb-10': true,
      'pt-4': true,
      'bg-teal-lightest': false,
      'bg-indigo-darker': true,
    });
    return (
      <div className="relative px-10 bg-indigo-darker">
        <div>
            <h4 className="vertical text-teal">Works</h4>
            <h2 className="section-title text-left text-white">Portfolio</h2>
        </div>
        <section className={classes}>
          {this.renderPortfolioList()}
        </section>
      </div>
    )
  }
}

const mapStateToProps = (state: any) => ({
  portfolios: state.portfolio.portfolios,
  scrolled: state.scroll.scrolled
});

const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators(ActionCreators, dispatch)
);

export const Portfolio = connect(mapStateToProps, mapDispatchToProps)(PortfolioComponent);