import * as React from 'react';
import sw from '../../../assets/images/sw.jpg';

// Drawing with text. Ported from Generative Design book - http://www.generative-gestaltung.de - Original licence: http://www.apache.org/licenses/LICENSE-2.0

// Application variables
const minFontSize = 3;
// const angleDistortion = 0;
const letters = "There was a table set out under a tree in front of the house, and the March Hare and the Hatter were having tea at it: a Dormouse was sitting between them, fast asleep, and the other two were using it as a cushion, resting their elbows on it, and talking over its head. 'Very uncomfortable for the Dormouse,' thought Alice; 'only, as it's asleep, I suppose it doesn't mind.'";


export default class Image extends React.Component {
  private canvas: HTMLCanvasElement;
  private canvasContext: any;
  private mouse = {x: 0, y: 0, down: false};
  private position = {x: 0, y: window.innerHeight/2};
  private counter = 0;

  constructor(props: any) {
    super(props);
    this.mouseMove = this.mouseMove.bind(this);
    this.draw = this.draw.bind(this);
    this.distance = this.distance.bind(this);
    this.mouseDown = this.mouseDown.bind(this);
    this.mouseUp = this.mouseUp.bind(this);
    this.doubleClick = this.doubleClick.bind(this);
    this.textWidth = this.textWidth.bind(this);
  }

  public componentDidMount() {
    this.init();
  }

  public render() {
    return (
      <div className="full-image-wrapper" style={{backgroundImage: 'url('+sw+')'}}>
        <div className="overflow">
          <canvas id="draw"/>
        </div>
      </div>
    )
  }

  private init() {
    this.canvas = document.getElementById('draw') as HTMLCanvasElement;
    this.canvasContext = this.canvas.getContext('2d');
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;

    this.canvas.addEventListener('mousemove', this.mouseMove, false);
    this.canvas.addEventListener('mousedown', this.mouseDown, false);
    this.canvas.addEventListener('mouseup',   this.mouseUp,   false);
    this.canvas.addEventListener('mouseout',  this.mouseUp,  false);
    this.canvas.addEventListener('dblclick', this.doubleClick, false);

    window.onresize = (event) => {
      this.canvas.width = window.innerWidth;
      this.canvas.height = window.innerHeight;
    }
  }

  private mouseMove (event: any){
    this.mouse.x = event.pageX;
    this.mouse.y = event.pageY;
    this.draw();
  }

  private draw() {
    if (this.mouse.down) {
      const d = this.distance(this.position, this.mouse);
      const fontSize = minFontSize + d/2;
      const letter = letters[this.counter];
      const stepSize = this.textWidth( letter, fontSize );

      if (d > stepSize) {
        const angle = Math.atan2(this.mouse.y - this.position.y, this.mouse.x - this.position.x);
        this.canvasContext.font = fontSize + "px Georgia";
        this.canvasContext.save();
        this.canvasContext.translate(this.position.x, this.position.y);
        this.canvasContext.rotate( angle );
        this.canvasContext.fillText(letter,0,0);
        this.canvasContext.restore();

        this.counter++;
        if (this.counter > letters.length-1) {
          this.counter = 0;
        }
        this.position.x = this.position.x + Math.cos(angle) * stepSize;
        this.position.y = this.position.y + Math.sin(angle) * stepSize;
      }
    }
  }

  private distance(pt: any, pt2: any){
    let xs = 0;
    let ys = 0;
    xs = pt2.x - pt.x;
    xs = xs * xs;
    ys = pt2.y - pt.y;
    ys = ys * ys;
    return Math.sqrt( xs + ys );
  }

  private mouseDown(event: any){
    this.mouse.down = true;
    this.position.x = event.pageX;
    this.position.y = event.pageY;
  }

  private mouseUp(event: any){
    this.mouse.down = false;
  }

  private doubleClick(event: any) {
    this.canvas.width = this.canvas.width;
  }

  private textWidth(str: any, size: any) {
    this.canvasContext.font = size + "px Georgia";

    if (this.canvasContext.fillText) {
      return this.canvasContext.measureText(str).width;
    } else if (this.canvasContext.mozDrawText) {
      return this.canvasContext.mozMeasureText(str);
    }

  }
}
