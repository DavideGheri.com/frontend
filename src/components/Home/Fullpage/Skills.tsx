import * as React from 'react';
import { shuffleArray } from '../../../utilities';
import Typist from 'react-typist';


const words = ['Web', 'React', 'Php', 'Frontend', 'Backend'];
// const words = ['Web', 'React'];

interface SkillsState {
  skills: string[],
  skill: string;
  oldSkill: string;
  i: number;
  typing: boolean;
}

export class Skills extends React.Component<any, SkillsState> {
  private timeout: any;

  constructor(props: any) {
    super(props);
    const skills = shuffleArray(words);
    this.state = {
      skills,
      skill: skills[0],
      oldSkill: '',
      i: 1,
      typing: true,
    }
    this.onTypingDone = this.onTypingDone.bind(this);
  }

  public componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  public render() {
    const { skill, typing } = this.state;
    return (
      <h2>
        <span className="typings inline-block">
          {typing && <Typist onTypingDone={this.onTypingDone}>
            <Typist.Delay ms={500} />
            {skill}
          </Typist>}
        </span> Developer
      </h2>
    )
  }

  private onTypingDone() {
    this.timeout = setTimeout(() => {
      const { skills, i } = this.state;
      let newI = i;
      let newSkill;
      if (skills[i]) {
        newSkill = skills[i];
        newI++;
      } else {
        newSkill = skills[0];
        newI = 1;
      }
      this.setState({skill: newSkill, i: newI, typing: false}, () => this.setState({typing: true}));
    }, 3100);
  }

}