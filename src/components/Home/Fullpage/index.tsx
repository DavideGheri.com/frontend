import * as React from 'react';
// import Image from './Image';
import { Stars } from './Stars';
import { Skills } from './Skills';

export const Fullpage = (props: any): JSX.Element => {
  return (
    <div style={{position: 'relative', height: '100vh'}}>
      {/*<Image/>*/}
      <div className="hp-title">
        <h1>Davide Gheri</h1>
        {/*<h2>Web developer</h2>*/}
        <Skills/>
      </div>
      <Stars/>
    </div>
  )
}