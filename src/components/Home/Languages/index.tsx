import * as React from 'react';
import { connect } from 'react-redux';
import {  bindActionCreators, Dispatch } from 'redux';
import * as ActionCreators from '../../../actions';
import { GlobalProps, GlobalState } from '../../../models/states';
import { Logo } from './Logo';
import { SkillModel, Skills } from '../../../models/skill';

interface Props extends GlobalProps {
  languages: {
    client: Skills,
    server: Skills
  };
}

class LanguagesComponent extends React.Component<Props, {}> {
  constructor(props: any) {
    super(props);
  }

  public componentWillMount() {
    this.props.getLanguages();
  }

  public render() {
    return (
      <section className="bg-teal-dark py-3 text-white relative">
        <div className="relative px-10">
          <h4 className="vertical text-indigo-darker">Skills</h4>
          <h2 className="pb-2 text-3xl section-title">Languages</h2>
        </div>
        <div className="flex flex-wrap px-3">
          <div className="w-full md:w-1/2 ">
            {/*<h3 className="text-center text-2xl border-b md:mr-3 pb-1">Client</h3>*/}
            <div className="flex flex-wrap">
              {this.props.languages.client.map((language: SkillModel) => (
                <Logo {...language} key={language.id}/>
              ))}
            </div>
          </div>
          <div className="w-full md:w-1/2">
            {/*<h3 className="text-center text-2xl border-b md:ml-3 pb-1">Server</h3>*/}
            <div className="flex flex-wrap">
              {this.props['languages'].server.map((language: SkillModel) => (
                <Logo {...language} key={language.id}/>
              ))}
            </div>
          </div>
        </div>
      </section>
    )
  }

}

const mapStateToProps = (state: GlobalState) => ({
  languages: state.language.languages
});

const mapDispatchToProps = (dispatch: Dispatch) => (
  bindActionCreators(ActionCreators, dispatch)
);

export const Languages = connect(mapStateToProps, mapDispatchToProps)(LanguagesComponent);