import * as React from 'react';
// import InlineSVG from 'react-svg-inline';
import SVG from 'react-inlinesvg';
import * as classNames from 'classnames';
import { SkillModel } from '../../../models/skill';

export const Logo = (props: SkillModel) => {
  const classes = classNames({
    'logo': true,
    'logo-lg': props.title.toLowerCase() === 'nginx'
  });

  return (
    <div className={classes}>
      {/*<InlineSVG svg={props.fields.image} style={{width: '100%'}}/>*/}
      <SVG src={props.fields.image.full_url}/>
      {/*<div className="overlay">*/}
        {/*<h5>{props.name}</h5>*/}
        {/*<p className="text-grey-lighter">{props.description}</p>*/}
        {/*<a href={props.link} target="_blank">{props.link}</a>*/}
      {/*</div>*/}
    </div>
  )
};