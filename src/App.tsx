import * as React from 'react';
import { createStore, applyMiddleware, Store } from 'redux';
import { Provider } from 'react-redux';
import createHistory from 'history/createBrowserHistory';
import { Route, Switch } from 'react-router';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import reducer from './reducers';
import { Home, About, Navbar, Footer, ScrollTop, Portfolio } from './components';
import { scrollDown, scrollUp } from './actions';
import thunk from 'redux-thunk';
import Helmet from 'react-helmet';

const history = createHistory();
const middleware = routerMiddleware(history);
const store: Store = createStore(
  reducer,
  applyMiddleware(middleware, thunk)
);

export default class App extends React.Component {

  private oldScroll: number = 0;

  public componentDidMount() {
    window.addEventListener('scroll', this.onScroll)
  }

  public componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll);
  }

  public render(): JSX.Element {
    return (
      <Provider store={store}>
        <div>
          <Helmet titleTemplate="%s - Davidegheri" defaultTitle="Davidegheri"/>
          <Navbar/>
          <ConnectedRouter history={history}>
            <ScrollTop>
              <Switch>
                <Route exact={true} path="/" component={Home}/>
                <Route path="/about" component={About}/>
                <Route path="/portfolio/:id" component={Portfolio}/>
              </Switch>
            </ScrollTop>
          </ConnectedRouter>
          <Footer/>
        </div>
      </Provider>
    )
  }

  private onScroll(e: Event) {
    const document = e.target as Document;
    const scroll = document.documentElement.scrollTop;
    if (scroll >= this.oldScroll) {
      store.dispatch(scrollDown(scroll))
    } else {
      store.dispatch(scrollUp(scroll))
    }
    this.oldScroll = scroll;
  }
}