import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

window['random'] = (min: number, max: number) => (
  Math.floor(Math.random() * (max - min + 1) + min)
);

const overlay = document.getElementById('overlay') as HTMLElement;

setTimeout(() => {
  ReactDOM.render(
    <App />,
    document.getElementById('root') as HTMLElement,
    () => {
      if (overlay) {
        overlay.getElementsByClassName('text')[0].classList.remove('hide');
        setTimeout(() => {
          overlay.classList.add('faded');
        }, 0)
      }
    }
  );
}, 0);
registerServiceWorker();
