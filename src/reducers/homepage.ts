import { Action } from '../models';
import * as types from '../actionTypes';
import { Homepage } from '../models/states';

const initialState: Homepage = {
  content: null
};

export const homepage = (state = initialState, action: Action): Homepage => {
  switch (action.type) {
    case types.HOMEPAGE_CONTENT_FETCHED:
      return Object.assign({}, state, {content: action.payload});

    default:
      return state;
  }
};
