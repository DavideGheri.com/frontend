import { Action } from '../models';
import * as types from '../actionTypes';
import { Scroll } from '../models/states';

const initialState: Scroll = {
  scrolled: false,
  offset: 0,
};

export const scroll = (state = initialState, action: Action): Scroll => {
  switch (action.type) {
    case types.SCROLL_DOWN:
    case types.SCROLL_UP:
      return Object.assign({}, state, action.payload);
    default:
      return state;
  }
};
