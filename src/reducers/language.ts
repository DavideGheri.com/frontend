import { Action } from '../models';
import * as types from '../actionTypes';
import { Language } from '../models/states';
import { SkillModel, SkillType } from '../models/skill';

const initialState: Language = {
  languages: {
    client: [],
    server: [],
  }
};

const TAXONOMY = 'skill-types';

export const language = (state = initialState, action: Action): Language => {
  switch (action.type) {
    case types.LANGUAGE_FETCHED:

      const client = action.payload.filter((skill: SkillModel) => {
        return skill.terms[TAXONOMY][0].title.toLowerCase() === SkillType.client;
      });
      const server = action.payload.filter((skill: SkillModel) => {
        return skill.terms[TAXONOMY][0].title.toLowerCase() === SkillType.server;
      });

      return Object.assign({}, state, {languages: {client, server}});
    default:
      return state;
  }
};
