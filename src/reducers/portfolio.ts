import { Action } from '../models';
import * as types from '../actionTypes';
import { Portfolio } from '../models/states';

const initialState: Portfolio = {
  portfolios: [],
  active: null,
  next: null,
  prev: null,
};

export const portfolio = (state = initialState, action: Action): Portfolio => {
  switch (action.type) {
    case types.PORTFOLIO_FETCHED:
      return Object.assign({}, state, {portfolios: action.payload});
    case types.PORTFOLIO_FOUND:
      return Object.assign({}, state, {active: action.payload});
    case types.NEXT_PREV_FOUND:
      return Object.assign({}, state, ...action.payload);
    default:
      return state;
  }
};
