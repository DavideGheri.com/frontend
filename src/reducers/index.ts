import { combineReducers } from 'redux';
import { scroll } from './scroll';
import { portfolio } from './portfolio';
import { language } from './language';
import { homepage } from './homepage';
import { routerReducer } from 'react-router-redux';

const reducer = combineReducers({
  scroll,
  portfolio,
  language,
  homepage,
  router: routerReducer
});

export default reducer;
