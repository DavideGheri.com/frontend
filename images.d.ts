declare module '*.svg'
declare module '*.png'
declare module '*.jpg'
declare module '*.jpeg'
declare module '*.gif'


declare module 'svg-inline-react'
declare module 'react-svg-inline'
declare module 'react-inlinesvg'
declare module 'react-typist'
